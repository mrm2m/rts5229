/* Driver for Realtek PCI-Express card reader
 *
 * Copyright(c) 2015 Moritz Rosenthal <moritz@mekelburger.org>  
 * Copyright(c) 2009 Realtek Semiconductor Corp. All rights reserved.  
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http:
 *
 * Authors:
 *   Moritz Rosenthal <moritz@mekelburger.org>
 *   wwang (wei_wang@realsil.com.cn)
 *   No. 450, Shenhu Road, Suzhou Industry Park, Suzhou, China
 */

#ifndef __RTSX_GENERAL_H
#define __RTSX_GENERAL_H

#include "rtsx.h"

#if DBG
extern int trigger_enabled;
#endif

int bit1cnt_long(u32 data);

#endif 

