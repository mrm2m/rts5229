General Information
===================

Linux driver for Realtek PCI-Express card reader chips RTS5227 and RTS5229.


Build Steps
===========

1) make
2) make install
3) depmod
4) reboot your computer

Note: Root privilege is required in step 2 and 3
